import './App.css';
import {
    Route,
    Switch,
    withRouter
  } from "react-router-dom";
import RegisterPage from './Components/RegisterPage';
import HomePage from './Components/HomePage';
import NotFoundPage from './Components/NotFoundPage';
import AuthorizePage from './Components/AuthorizePage';

function App(props) {
    const { history } = props;

    return (
    <div className="App">
        <Switch>
            <Route exact history={history} path="/login" component={AuthorizePage}/>
            <Route exact history={history} path="/register" component={RegisterPage}/>
            <Route exact history={history} path="/home" component={HomePage}/>
            <Route component={NotFoundPage}/>
        </Switch>
    </div>
  );
}

export default withRouter(App);
