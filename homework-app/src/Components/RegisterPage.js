import React from 'react';
import { register, setState } from '../Reducers/registerReducer';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useSelector, useDispatch } from 'react-redux'

function RegisterPage(props) {
    const { email, password, passwordRepeat, name, lastName } = useSelector((state) => state.login);
    const dispatch = useDispatch();

    const handleChange = e => {
        dispatch(setState({id: e.currentTarget.id, value: e.currentTarget.value}));
    }

    const handleRegister = () => {
        if (password === passwordRepeat){
            dispatch(register());
        }
    }

    return <div className="form">
        <div className="textbox">
            <TextField
                label="Email"
                id="email"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="text"
                value={email}
            />
        </div>
        <div className="textbox">
            <TextField 
                label="Password"
                id="password"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="password"
                value={password}
            />
        </div>
        <div className="textbox">
            <TextField 
                label="Repeat Password"
                id="passwordRepeat"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="password"
                value={passwordRepeat}
            />
        </div>
        <div className="textbox">
            <TextField
                label="Name"
                id="name"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="text"
                value={name}
            />
        </div>
        <div className="textbox">
            <TextField
                label="LastName"
                id="lastName"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="text"
                value={lastName}
            />
        </div>
        <Button type="button" color="primary" className="form__custom-button" onClick={e => handleRegister()}>
        Register
      </Button>
    </div>
}

export default RegisterPage;