import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { login, setState } from '../Reducers/loginReducer';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function AuthorizePage() {
    useSelector((state) => console.log(state))
    const { email, password } = useSelector((state) => state.login);
    const dispatch = useDispatch();

    const handleChange = e => {
        dispatch(setState({id: e.currentTarget.id, value: e.currentTarget.value}));
    }

    return <React.Fragment>
    <form className="form">
        <div className="textbox">
            <TextField
                label="Email"
                id="email"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="text"
                value={email}
            />
        </div>
        <div className="textbox">
            <TextField 
                label="Password"
                id="password"
                formControlProps={{
                fullWidth: true
                }}
                handleChange={handleChange}
                type="password"
                value={password}
            />
        </div>
      <Button type="button" color="primary" className="form__custom-button" onClick={e => dispatch(login())}>
        Log in
      </Button>
    </form>
  </React.Fragment>
}

export default AuthorizePage;