import React from 'react';
import Typography from '@mui/material/Typography';

function NotFoundPage(props) {
    return <div>
        <Typography variant="h1" component="div" gutterBottom>
            Page not found :(
        </Typography>
    </div>
}

export default NotFoundPage;