import { configureStore } from '@reduxjs/toolkit'
import loginReducer from './Reducers/loginReducer'
import registerReducer from './Reducers/registerReducer'

export default configureStore({
  reducer: {
      login: loginReducer,
      register: registerReducer
  },
})