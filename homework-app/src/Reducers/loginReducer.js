import { createSlice } from '@reduxjs/toolkit'

export const loginReducer = createSlice({
    name: 'login',
    initialState: {
      email: null,
      password: null
    },
    reducers: {
        login: (state) => {
            fetch("api/auth", {
                method: "POST",
                body: state
            })
        },
        setState: (state, action) => {
            state[action.payload.id] = action.payload.value;
        } 
    },
  })
  
  export const { login, setState } = loginReducer.actions
  
  export default loginReducer.reducer

  