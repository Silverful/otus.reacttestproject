import { createSlice } from '@reduxjs/toolkit'

export const registerReducer = createSlice({
    name: 'register',
    initialState: {
      email: null,
      password: null,
      passwordRepeat: null,
      name: '',
      lastname: ''
    },
    reducers: {
        register: (state) => {
            fetch("api/register", {
                method: "POST",
                body: state
            })
        },
        setState: (state, action) => {
            state[action.payload.id] = action.payload.value;
        } 
    },
  })
  
  export const { register, setState } = registerReducer.actions
  
  export default registerReducer.reducer
  